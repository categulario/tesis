\begin{chapter}{Puntos de equilibrio}

En este capítulo se estudian los puntos de equilibrio en el sentido de Nash de los juegos topológicos estudiados en los capítulos previos, es decir, conjuntos de estrategias para cada jugador que maximizan las ganancias de todos en un juego de información perfecta.

\begin{section}{Algunas definiciones}

\begin{definition}
Si en $\game = (X, \Gamma, N, \{f_i\}_{i \in N})$ tomamos un subconjunto $A$ de $X$ y las restricciones a $A$ de $\Gamma$ y las $f_i$ entonces $\game |_A = (A, \Gamma |_A, N, \{f_i |_A\}_{i \in N})$ es un subjuego de $\game$.
\end{definition}

\begin{theorem}
Todo subjuego de un juego es un juego.
\end{theorem}

\begin{proof}
Realmente ninguna de las cuatro condiciones para tener un juego topológico se ve afectada por la restricción a $A$, así que un subjuego de un juego es un juego también.
\end{proof}

Para poder definir un punto de equilibrio es necesario introducir un poco de notación. Si $\sigma = (\sigma_1, \sigma_2, \dots, \sigma_N)$ y $\tau_i$ es una estrategia para el jugador $(i)$ entonces
$$
\sigma/\tau_i = (\sigma_1, \sigma_2, \dots, \sigma_{i-1}, \tau_i, \sigma_{i+1}, \dots, \sigma_N)
$$

\begin{definition}
Un \emph{punto de equlibrio} para el punto $x_0$ es una tupla de estrategias $\sigma = (\sigma_1, \sigma_2, \dots, \sigma_N)$ tal que
$$
f_i(x_0; \sigma/\tau_i) \leq f_i(x_0; \sigma)
$$
para todas $\tau$ e $i$. De la misma manera $\sigma$ es un punto de equilibrio del juego si además esto se cumple para cada $x \in X$.
\end{definition}

Es decir, si se reemplaza alguna de las estrategias de alguno de los jugadores entonces su ganancia es menor o igual. Dicho de otra manera, un \emph{punto de equilibrio} es un conjunto de estrategias (una para cada jugador) tal que todos ganan lo más que pueden ganar en conjunto.

\end{section}

\begin{section}{Existencia}

Para el siguiente teorema se va usar $\argmax$ para especificar que, a diferencia de $\max$, el resultado es alguno de los argumentos que maximiza la función y no el valor de la función en si.

\begin{theorem}
\label{thm:zermelo}
(\emph{Zermelo-von Newmann}) Si un juego es localmente finito y los conjuntos $F_i = \{f_i(x) | x \in X \}$ son finitos entonces existe un punto de equilibrio.
\end{theorem}

\begin{proof}
Sea $\bar{\sigma}^\alpha$ la restricción a $X(\alpha)$ de $\sigma$ (también podría escribirse $\sigma |_{X(\alpha)}$). Definimos $\bar{\sigma}^0$ como
$$
\bar{\sigma}^0 x = x \qquad \forall x \in X(0).
$$
Ahora veremos que si se ha definido $\bar{\sigma}^k$ podemos definir $\bar{\sigma}^{k+1}$ de la siguiente manera:
$$
\bar{\sigma}^{k+1} x = \left\{
	\begin{array}{lr}
		\bar{\sigma}^k x & \text{si } x \in X(k) \\
		\displaystyle\argmax_{z \in \Gamma x} f_j(z; \bar{\sigma}^k) & \text{si } x \in X(k+1)\minus X(k), x \in X_j
	\end{array}
\right.
$$

Es obvio que $\bar{\sigma}^0$ es un punto de equilibrio del subjuego definido por $X_0$ pues $\Gamma x = \phi$ si $x \in X(0)$. Queda demostrar que si $\bar{\sigma}^k$ es un punto de equlibrio de $X(k)$, $\bar{\sigma}^{k+1}$ es un punto de equlibrio de $X(k+1)$. Es decir
$$
f_i(x; \bar{\sigma}^{k+1}/\bar{\tau}_i^{k+1}) \leq f_i(x; \bar{\sigma}^{k+1})
$$
para todas $i$, $x \in X(k+1)$ y $\bar{\tau}^{k+1}$. Si $x \in X(k)$ esto es cierto pues $\bar{\sigma}^k$ es un punto de equilibrio. Supóngase que $x \in X(k+1)\minus X(k) \intersection X_i$ y considérese una estrategia $\tau_i$ para $(i)$. Tenemos que
$$
f_i(\z; \sigma/\tau_i) \leq f_i(\z; \sigma) \leq f_i(\y; \sigma).
$$
Donde la primera desigualdad se cumple ya que $\z \in X(k)$ y la segunda es cierta porque $\sigma_i$ es la estrategia para el $i$-ésimo jugador del punto de equilibrio $\sigma$ que estamos construyendo. Esta desigualdad se va a usar en los siguientes cuatro casos.\\

Si $(i) \in N^+$ y $f_i(x) \leq f_i(\z; \sigma/\tau_i)$, tenemos
$$
f_i(x; \sigma/\tau_i) = f_i(\z; \sigma/\tau_i) \leq f_i(\y; \sigma) \leq f_i(x; \sigma).
$$

Si $(i) \in N^+$ y $f_i(x) \geq f_i(\z; \sigma/\tau_i)$, tenemos
$$
f_i(x; \sigma/\tau_i) = f_i(x) \leq f_i(x; \sigma)
$$

Si $(i) \in N^-$ y $f_i(x) \geq f_i(\y; \sigma)$, tenemos
$$
f_i(x; \sigma/\tau_i) \leq f_i(\z; \sigma/\tau_i) \leq f_i(\y; \sigma) = f_i(x; \sigma)
$$

Si $(i) \in N^-$ y $f_i(x) \leq f_i(\y; \sigma)$, tenemos
$$
f_i(x; \sigma/\tau_i) \leq f_i(x) = f_i(x; \sigma)
$$
Como se puede observar, siempre se cumple que $f_i(x; \sigma/\tau_i) \leq f_i(x; \sigma)$. Si $x \in X_j$, $j \neq i$, entonces la estrategia $\tau_i$ del jugador $(i)$ no va a afectar la ganancia del jugador $(j)$ (a quien pertenece el turno pues $x \in X_j$) y se tienen la misma conclusión.\\

Finalmente, si $\alpha$ es un ordinal límite y para cada $\beta < \alpha$ se tiene que $\bar{\sigma}^\beta$ es un punto de equilibrio de $X(\beta)$ se puede definir $\bar{\sigma}^\alpha$ como
$$
\bar{\sigma}^\alpha x = \bar{\sigma}^\beta x, \text{ si } x \in X(\beta)
$$
Así por inducción transfinita se puede definir $\bar{\sigma}^\alpha$ para cada $\alpha$ y como el juego es localmente finito entonces $X = X(\alpha)$ para algún alpha (Teorema \ref{thm:local_finito}) y $\bar{\sigma}^\alpha$ es un punto de equilibrio para el juego definido en $X$.
\end{proof}

\begin{corolary}
\label{crl:zermelo}
Todo juego localmente finito entre dos jugadores en el que no puede haber empates está determinado. Es decir, existe una estrategia ganadora para uno (y solo uno) de los jugadores.
\end{corolary}

\begin{proof}
Podemos describir este juego en términos del teorema anterior si definimos las funciones $f_i$ con $i \in \{1, 2\}$ de los jugadores como $1$ para posiciones definidas como ganadoras para el jugador $(i)$ y $0$ en otro caso. Como el juego no puede terminar en empate, si $x$ es la posición final, se cumple ya sea $f_1(x) =1$ y $f_2(x) =0$ ó $f_1(x) = 0$ y $f_2(x) = 1$.\\

Dado que el conjunto de las imágenes de cada $f_i$ es finito, el teorema \ref{thm:zermelo} garantiza que existe un punto de equilibrio $\sigma = (\sigma_1, \sigma_2)$ para el juego. El juego no puede terminar en empate así que una partida jugada con el punto de equilibrio como conjunto de estrategias para cada jugador le dará la victoria a uno de los dos. Por la definición de punto de equilibrio, si este jugador juega siempre esa estrategia, sin importar qué haga el oponente, va a ganar. Esa estrategia es la estrategia ganadora.
\end{proof}

Ahora se establecerá un resultado que relaciona el recién demostrado \emph{Teorema de Zermelo-von Newmann} con el Teorema \ref{thm:opt_strategies}, conectando así sus ideas con la variante topológica expuesta en este trabajo.

\begin{corolary}
\label{crl:bridge}
Las hipótesis del Teorema \ref{thm:opt_strategies} implican la conclusión del Teorema \ref{thm:zermelo}\footnote{De mi orgullosa autoría.}
\end{corolary}

\begin{proof}
Para demostrar esto se tomará un juego que cumple las condiciones del Teorema \ref{thm:opt_strategies} y veremos que podemos llegar a la conclusión del Teorema \ref{thm:zermelo}.\\

Supóngase que tenemos un juego localmente finito y topológico superior para todos los jugadores $(i) \in N$, tal que $X_0$ es abierto en $X$ y que $\Gamma x$ es compacto para toda $x \in X$. Sea $(i)$ in $N$, si $(i) \in N^+$, como el juego es topológico superior para $(i)$, localmente finito y $X_0$ es un conjunto abierto en $X$ entonces el Teorema \ref{thm:opt_strategies} dice que existe una estrategia $\sigma_i$ que le garantiza a $(i)$ una ganancia $\phi_i(x)$\footnote{El subíndice indica que la función a usar para la definición de $phi(x)$ es $f_i$.}. Si $(i) \in N^-$, como el juego es topológico superior para $(i)$ el mismo teorema garantiza la existencia de de una estrategia $\sigma_i$.\\

Lo que se acaba de decir es que existen estrategias óptimas para todos los $(i) \in N$, con lo cual se puede formar una tupla $\sigma = (\sigma_1, \sigma_2, \dots, \sigma_n)$ que resulta ser un punto de equilibrio. En efecto, si sustituímos alguna de las estrategias, digamos la $i$-ésima, por $\tau_i$ y el juego comienza desde $x_0$, como la estrategia $\sigma_i$ le permite a $(i)$ garantizar $\phi_i(x_0)$, que es lo más que puede garantizar desde $x_0$ entonces
$$
f_i(x_0; \sigma/\tau_i) \leq f_i(x_0; \sigma).
$$
Para toda $(i) \in N$ y para toda $x_0 \in X$.
\end{proof}

Llegada a esta conclusión podemos establecer un corolario casi idéntico al anterior para juegos en los que las $f_i$ estén definidas como 1 si el jugador gana, $0$ en cualquier otro caso.

\end{section}

\end{chapter}
